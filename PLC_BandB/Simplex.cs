﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLC_BandB
{
    /* SIMPLEX:
    * ( (i) - szczególne przypadki, (*) pętla )
    * gdy y jest dopuszczalne to:
    *    (1)jeżeli y(0,k)<0 && y(i,k)<=0 dla k=1..n, jakieś i to
    *        - STOP-> ROZWIĄZANIE = NIEOGRANICZONE
    *    
    *    (*)jeżeli optymalna to:
    *        - STOP-> ROZWIĄZANIE = odczytać z tablicy
    *    (PRYMALNA TABLICA SIMPLEKSU)
    *    - znajdź x wchodzące do bazy - takie k, że max |y(0,k)| k>0    == min(y(0,k)<0)
    *    - znajdź x wychodzące z bazy - takie i, że min (y(i,0)/y(i,k)) ale y(i,k)>0
    *    - eliminacja Gaussa - ze wzoru
    *    (2)jeżeli dopuszczalna i optymalna i y(0,j0)=0 i y(i0,0)>0 i y(i0,j0)>0 to:
    *        - STOP-> ROZWIĄZANIE = nieskończenie wiele rozwiązań na ograniczonym
    *    (3)jeżeli dopuszczalna i optymalna i y(0,j0)=0 i y(i,0)=0 lub y(i,j0)<=0 to:
    *        - STOP-> ROZWIĄZANIE = nieskończenie wiele rozwiązań na nieograniczonym
    *    -> (*)
    *    
    * (4)
    * gdy y nie jest dopuszczalne to:
    *    (DUALNA METODA SIMPLEKSU)  
    *    - znajdź taki wiersz s dla którego y(s,0)<0 oraz min(y(s,0))
    *    (*)
    *    - znajdź x wchodzące do bazy - takie k, że max |y(s,k)| k>0    == min(y(s,k)<0)
    *    jeżeli brak takiego x (czyli y(s,k)>=0) to:
    *        - STOP-> ROZWIĄZANIE =  sprzeczność
    *    - znajdź x wychodzące z bazy - takie i, że min (y(i,0)/y(i,k)) ale y(i,k)>0
    *      (można pominąć wiersz s)
    *    - eliminacja Gaussa
    *    jeżeli optymalna to:
    *        - STOP-> ROZWIĄZANIE = odczytać z tablicy
    *    (*)
    */  
    class Simplex
    {
        public int id;
        public int M, N, col, row;
        private decimal[,] y0;
        public decimal[,] y;
        public string res;
        public Simplex (decimal[,] t)
        {
            y0 = t;
            y = t;
            row = y.GetLength(0);
            col = y.GetLength(1);
            M = row - 2;
            N = col - 2;
        }
        public decimal[,] GetProblem()
        {
            return y0;
        }
        public Problem Solve()
        {
            //MessageBox.Show("\n"+this.ToString()+" " +y[2,1].ToString()+ " " + y[2,2].ToString() + " " + y[2, 3].ToString());
            string str = "";
            decimal[] w;
            // metoda dwufazowa
            int xwe, xwy;
            while (!Feasibility())
            {
                int s = 0;
                decimal miny = 0;
                for (int i = 2; i < row; i++)
                    if (y[i, 1] < miny)
                    {
                        miny = y[i, 1];
                        s = i;
                    }

                if (s != 0)  //to nie jest dopuszczalna
                {
                    xwe = FindXin(s);
                    if (xwe == 0) return new Problem() { y = y, info = "ZP" }; //ok
                    xwy = FindXout(xwe, s);
                    if (xwy == 0) return new Problem() { y = y, info = "ZP-ERROR1" };
                    y = Gauss(xwe, xwy);
                    //MessageBox.Show(this.ToString());
                }
                
            }
            // dopuszczalne - sprawdzić czy nieograniczone = brak rozwiązań
            for (int k = 1; k < col; k++) 
                if (y[1, k] < 0)
                {
                    int j = 0;
                    for (int i = 2; i < row; i++)
                        if (y[i, k] <= 0) j++;
                    if (j == M) str += "NN-ZP"; 
                }
            //czy wiersz sprzeczny b<=0, a>0 (xk = b + aixi + ajxj + ..., xk, xi, xj > 0)
            for (int i = 2; i < row; i++)
                if (y[i, 1] < 0)
                {
                    int j = 0, k = 0;
                    for (int c = 2; c < col; c++)
                        if (y[i, c] > 0) j++;
                        else if (y[i, c] == 0) k++;
                    if (j == N|| ((j + k) == N && j > 0)) str += "ZP";
                }
            if (str.Length > 0)
            {
                w = GetSolution();
                return new Problem() { y = y, x0 = w[0], info = str, p = w.Skip(1).ToList() };
            }
            while (!Optimality())
            {
                xwe = FindXin(1);
                if (xwe == 0) return new Problem() { y = y, info = "ZP" };
                xwy = FindXout(xwe, 1);
                if (xwy == 0) return new Problem() { y = y, info = "ZP-ERROR2" };
                y = Gauss(xwe, xwy);
                for (int k = 1; k < col; k++)  //czy nieograniczone = brak rozwiązań
                    if (y[1, k] < 0)
                    {
                        int j = 0;
                        for (int i = 2; i < row; i++)
                            if (y[i, k] <= 0) j++;
                        if (j == M) str += "NN-ZP";
                    }
                //czy wiersz sprzeczny b<=0, x>0
                for (int r = 2; r < row; r++)
                    if (y[r, 1] <= 0)
                    {
                        int j = 0, k = 0;
                        for (int i = 2; i < col; i++)
                            if (y[r, i] > 0) j++;
                            else if (y[r, i] == 0) k++;
                        if (j == N || ((j + k) == N && j > 0)) str += "ZP";
                    }
                if (!Feasibility())
                    return new Problem() { y = y, info = "ZP-ERROR3" };
                
            }
            // optymalne - sprawdzić czy nieskończenie wiele na ogr. / nieogr.
            //!! (2) y(0,j0)=0 i y(i0,0)>0 i y(i0,j0)>0   => NW na ograniczonym
            //!! (3) y(0,j0)=0 i y(i,0)=0 lub y(i,j0)<=0  => NW na nieograniczonym
            str = "";
            for (int j = 2; j < col; j++) //ok
                if (y[1, j] == 0)
                {
                    bool nwoCheck = false;
                    bool nwnCheck = true;
                    for (int i = 2; i < row; i++)
                        if (y[i, j] > 0)
                        {
                            nwnCheck = false;
                            if (y[i, 1] > 0)
                                nwoCheck = true;
                        }
                    if (nwoCheck) str = "NWO";
                    else if (nwnCheck) str = "NWN";
                    else
                    { 
                        nwnCheck = true;
                        for (int i = 2; i < row; i++)
                            if (y[i, 1] != 0) nwnCheck = false;
                        if (nwnCheck) str = "NWN";
                    }
                    break;
                }
            w = GetSolution();
            return new Problem() { y = y, x0 = w[0], info=str, p = w.Skip(1).ToList() };
        }
        private bool Optimality() // optymalność y(0,i)>=0
        {
            for (int i = 2; i < col; i++) // x0
                if (y[1, i] < 0) return false;
            return true;
        }
        private bool Feasibility() // dopuszczalność y(i,0)>=0
        {
            for (int i = 2; i < row; i++) // b
                if (y[i, 1] < 0) return false;
            return true;
        }
        private int FindXin(int w)
        {
            int k = 0;
            decimal miny = Decimal.MaxValue;
            for (int i = 2; i < col; i++)
                if (y[w, i] < 0 && y[w, i] < miny)
                {
                    miny = y[w, i];
                    k = i;
                }
            return k;
        }
        private int FindXout(int k, int w)
        {
            int r = 0;
            decimal miny = Decimal.MaxValue;
            for (int i = 2; i < row; i++)
                if (Math.Round(y[i, k],20) != 0 &&//y[i, k] != 0 &&//
                    y[i, 1] / y[i, k] > 0 &&   // zmiana!! > na >= czy dobrze?
                    y[i, 1] / y[i, k] <= miny )//&& i != w)
                {
                    miny = y[i, 1] / y[i, k];
                    r = i;
                }
            return r;
        }
        private decimal[,] Gauss(int xwe, int xwy)
        {
            decimal[,] simp = new decimal[M + 2, N + 2];
            decimal p = y[xwy, xwe];
            for (int i = 1; i < col; i++)
            {
                for (int j = 1; j < row; j++)
                {
                    decimal w = 0;
                    if (i == xwe && j == xwy) w = 1 / p;
                    else if (i == xwe) w = -y[j, i] / p;
                    else if (j == xwy) w = y[j, i] / p;
                    else w = y[j, i] - (y[j, xwe] * y[xwy, i]) / p; 
                    simp[j, i] = w;
                }
            }
            for (int i = 2; i < col; i++) simp[0, i] = y[0, i];
            for (int j = 1; j < row; j++) simp[j, 0] = y[j, 0];
            simp[0, xwe] = y[xwy, 0];
            simp[xwy, 0] = y[0, xwe];
            return simp;
        }
        private decimal[] GetSolution()
        {
            decimal[] w = new decimal[N + M + 1];
            for (int i = 2; i <= N + 1; i++)
                w[Convert.ToInt32(y[0, i])] = 0;
            for (int i = 1; i < M + 2; i++)
                w[Convert.ToInt32(y[i, 0])] = y[i, 1];
            return w;
        }
        public void AddBound(int xIndex, decimal xLimit, bool minor) 
        {
            M++; row++;
            decimal[,] newY; int k = -1, ix = 0;
            newY = ResizeAndCopy(y, row, col);
            newY[row - 1, 0] = M + N;
            for (int j = 2; j < row - 1; j++)
                if (newY[j, 0] == xIndex) ix = j;
            if (minor)
                newY[row - 1, 1] = Math.Floor(xLimit) - newY[ix, 1];
            else
            {
                newY[row - 1, 1] = -Math.Ceiling(xLimit) + newY[ix, 1];
                k = 1;
            }
            for (int j = 2; j < col; j++)
                newY[row - 1, j] = k * newY[ix, j];
            y = newY;
            y0 = newY;
            //y0 = new decimal[row, col];
            //y0 = (decimal[,])ResizeArray(newY, new int[] { row, col }); ;
        }

        public override string ToString()
        {
            string output = "";
            for (int j = 0; j <= M + 1; j++)
            {
                for (int i = 0; i <= N + 1; i++)
                {
                    if (j == 0 && i == 0) output += "    ";
                    else if (j == 0 && i == 1) output += string.Format("  {0}         ", '-');
                    else if (j == 0) output += string.Format("{0}      ", 'x' + y[0, i].ToString());
                    else if (i == 0) output += string.Format("{0}  ", 'x' + y[j, 0].ToString());
                    else output += string.Format("{0:0.00}  ", y[j, i]); 
                }
                output += "\n";
            }
            return output;
        }
        public string ToString2()
        {
            string output = "";
            for (int j = 0; j <= M + 1; j++)
            {
                for (int i = 0; i <= N + 1; i++)
                {
                    if (j == 0 && i == 0) output += "    ";
                    else if (j == 0 && i == 1) output += string.Format("  {0}         ", '-');
                    else if (j == 0) output += string.Format("{0}      ", 'x' + y0[0, i].ToString());
                    else if (i == 0) output += string.Format("{0}  ", 'x' + y0[j, 0].ToString());
                    else output += string.Format("{0:0.00}  ", y0[j, i]);
                }
                output += "\n";
            }
            return output;
        }
        private decimal[,] ResizeAndCopy (decimal[,] array, int rows, int columns)
        {
            decimal[,] newArray = new decimal[rows, columns];
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < columns; j++)
                    if (i >= array.GetLength(0) || j >= array.GetLength(1))
                        newArray[i, j] = 0;
                    else newArray[i, j] = array[i, j];
            return newArray;
        }
        private static Array ResizeArray(Array arr, int[] newSizes)
        {
            if (newSizes.Length != arr.Rank)
                throw new ArgumentException("arr must have the same number of dimensions " +
                                            "as there are elements in newSizes", "newSizes");

            var temp = Array.CreateInstance(arr.GetType().GetElementType(), newSizes);
            int length = arr.Length <= temp.Length ? arr.Length : temp.Length;
            Array.ConstrainedCopy(arr, 0, temp, 0, length);
            return temp;
        }   

    }
}
