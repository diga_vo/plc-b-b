﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLC_BandB
{
    public partial class MainForm : Form
    {
        private bool english, maximal;
        private decimal epsilon;
        private decimal upDown1Value, upDown2Value;
        private int N, M; //N-parameters(N*2+3 colmns), M-restrictions(M+1 rows)
        private int columnWidth = 30, columnWidth2 = 25;
        private decimal[,] A;
        private int boxWidth = 35, boxHeight = 28;
        private Form diagramForm = new Form() { Text = "Diagram", BackColor = SystemColors.Window };
        private int dataGridHight;
        private List<RichTextBox> treeBoxes;
        public MainForm()
        {
            InitializeComponent();
            tableLayoutPanel.SetRowSpan(groupBox5,2);
            epsilon = 1M/1000;
            epsilonToolStripMenuItem.Text = epsilon.ToString();
            english = true;
            polskiToolStripMenuItem.PerformClick();
            maximal = true;
            this.MinimumSize = new Size(655, 300);
            this.ShowIcon = false;
            N = 1; M = 1;
            upDown1Value = 1; upDown2Value = 1;
            InitializeDataGrid();
            diagramForm.FormClosing += diagramForm_FormClosing;
            dataGridHight = dataGridView.Rows[0].Height;
            label7.Text = "";
        }
        // NUMERIC UP/DOWNS
        // changing numer of columns and rows
        // dataGridView1[iCol, iRow] !!
        //
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            // COLUMNS
            if (numericUpDown1.Value > upDown1Value)
            {
                if (checkBox1.Checked)
                {
                    this.numericUpDown2.ValueChanged -= numericUpDown2_ValueChanged;
                    this.numericUpDown2.Value++;
                    AddRemoveRow(true);
                    upDown2Value = numericUpDown2.Value;
                    this.numericUpDown2.ValueChanged += numericUpDown2_ValueChanged;
                }
                AddRemoveColumn(true);
            }
            else if (numericUpDown1.Value < upDown1Value)
            {
                if (checkBox1.Checked)
                {
                    this.numericUpDown2.ValueChanged -= numericUpDown2_ValueChanged;
                    this.numericUpDown2.Value--;
                    AddRemoveRow(false);
                    upDown2Value = numericUpDown2.Value;
                    this.numericUpDown2.ValueChanged += numericUpDown2_ValueChanged;
                }
                AddRemoveColumn(false);
            }
            upDown1Value = numericUpDown1.Value;
        }
        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            // ROWS
            if (numericUpDown2.Value > upDown2Value)
            {
                if (checkBox1.Checked)
                {
                    this.numericUpDown1.ValueChanged -= numericUpDown1_ValueChanged;
                    this.numericUpDown1.Value++;
                    AddRemoveColumn(true);
                    upDown1Value = numericUpDown1.Value;
                    this.numericUpDown1.ValueChanged += numericUpDown1_ValueChanged;
                }
                AddRemoveRow(true);
            }
            else if (numericUpDown2.Value < upDown2Value)
            {
                if (checkBox1.Checked)
                {
                    this.numericUpDown1.ValueChanged -= numericUpDown1_ValueChanged;
                    this.numericUpDown1.Value--;
                    AddRemoveColumn(false);
                    upDown1Value = numericUpDown1.Value;
                    this.numericUpDown1.ValueChanged += numericUpDown1_ValueChanged;
                }
                AddRemoveRow(false);
            }
            upDown2Value = numericUpDown2.Value;
        }
        private void AddRemoveRow(bool add)
        {
            if (add)
            {
                dataGridView.Rows.Add((DataGridViewRow)dataGridView.Rows[1].Clone());
                for (int i = 2; i <= upDown1Value * 2; i += 2)
                    dataGridView[i, (int)upDown2Value + 1].Value = dataGridView[i, (int)upDown2Value].Value;
                dataGridView[(int)upDown1Value * 2 + 1, (int)upDown2Value + 1].Value = dataGridView[(int)upDown1Value * 2 + 1, (int)upDown2Value].Value;
            }
            else
                dataGridView.Rows.RemoveAt((int)upDown2Value);
            dataGridView_RowHeightChanged(this, null);
        }
        private void AddRemoveColumn(bool add)
        {
            if (add)
            {
                this.dataGridView.Columns.Insert((int)upDown1Value * 2 + 1, (DataGridViewColumn)dataGridView.Columns[2].Clone());
                this.dataGridView.Columns.Insert((int)upDown1Value * 2 + 1, (DataGridViewColumn)dataGridView.Columns[1].Clone());
                for (int i = 0; i <= upDown2Value; i++)
                    dataGridView[((int)upDown1Value + 1) * 2,i].Value = "x" + (upDown1Value + 1).ToString();
            }
            else
            {
                this.dataGridView.Columns.RemoveAt(((int)upDown1Value - 1) * 2 + 1);
                this.dataGridView.Columns.RemoveAt(((int)upDown1Value - 1) * 2 + 1);
            }
            dataGridView_ColumnWidthChanged(this, null);
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox1.Checked)
            {
                this.checkBox1.Image = global::PLC_BandB.Properties.Resources.locked1;
                this.checkBox1.CheckedChanged -= checkBox1_CheckedChanged;
                this.checkBox1.Checked = false;
                for (int i = (int)upDown2Value; i < upDown1Value; i++)
                {
                    this.numericUpDown2.Value++;
                    numericUpDown2_ValueChanged(sender, e);
                }
                for (int i = (int)upDown2Value; i > upDown1Value; i--)
                {
                    this.numericUpDown2.Value--;
                    numericUpDown2_ValueChanged(sender, e);
                }
                this.checkBox1.Checked = true;
                this.checkBox1.CheckedChanged += checkBox1_CheckedChanged;
            }
            else this.checkBox1.Image = global::PLC_BandB.Properties.Resources.unlocked1;
        }

        // DATA GRID VIEW
        //
        void InitializeDataGrid()
        {
            this.dataGridView.ColumnCount = 5;
            this.dataGridView.Columns[0].Name = ""; this.dataGridView.Columns[0].ReadOnly = true;
            this.dataGridView.Columns[0].DefaultCellStyle.BackColor = Color.FromKnownColor(KnownColor.Control);
            this.dataGridView.Columns[1].Name = "a"; this.dataGridView.Columns[1].ValueType = typeof(decimal);
            this.dataGridView.Columns[2].Name = "x"; this.dataGridView.Columns[2].ReadOnly = true;
            this.dataGridView.Columns[3].Name = "s"; this.dataGridView.Columns[3].ReadOnly = true;
            this.dataGridView.Columns[4].Name = "b"; this.dataGridView.Columns[4].ValueType = typeof(decimal);
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.Columns[0].Width = columnWidth2 + 5;
            this.dataGridView.Columns[0].DefaultCellStyle.Font = new Font(dataGridView.DefaultCellStyle.Font.FontFamily, 8.25F, FontStyle.Bold);
            this.dataGridView.Columns[1].Width = columnWidth;
            this.dataGridView.Columns[2].Width = columnWidth2;
            this.dataGridView.Columns[2].DefaultCellStyle.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.dataGridView.Columns[3].Width = columnWidth2;
            this.dataGridView.Columns[3].DefaultCellStyle.Font = new Font(dataGridView.DefaultCellStyle.Font.FontFamily, 9F, FontStyle.Bold);
            this.dataGridView.Columns[4].Width = columnWidth;
            List<string> rows = new List<string>();
            rows.Add(""); rows.Add(null); rows.Add("x1"); rows.Add(""); rows.Add(null);
            this.dataGridView.Rows.Add(rows.ToArray());
            rows = new List<string>();
            rows.Add(""); rows.Add(null); rows.Add("x1"); rows.Add("≤"); rows.Add(null);
            this.dataGridView.Rows.Add(rows.ToArray());
            //this.dataGridView[N * 2 + 2, 0].ReadOnly = true;
            this.dataGridView.Size = new Size(2 * columnWidth + 3 * columnWidth2 + 5, 60);
            this.dataGridView[0, 0].Value = "max";
        }
        void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            if (e.RowIndex != 0) this.dataGridView[0,e.RowIndex].Value = (e.RowIndex).ToString();
        }
        private void dataGridView1_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }
        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column_KeyPress);
            string name = dataGridView.Columns[dataGridView.CurrentCell.ColumnIndex].Name;
            if (name == "a" || name == "b")
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Column_KeyPress);
                }
            }
        }
        private void Column_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)8) return;
            if (e.KeyChar == '.') e.KeyChar = ',';
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)
                && e.KeyChar != ',' && e.KeyChar != '-' && e.KeyChar != '/')
                e.Handled = true;
            if (e.KeyChar == ',' && ((sender as TextBox).Text.IndexOf(",") > -1 || !(sender as TextBox).Text.Any()))
                e.Handled = true;
            if (e.KeyChar == '-' && ((sender as TextBox).Text.IndexOf("-") > -1  || (sender as TextBox).Text.Any()))
                e.Handled = true;
            if (e.KeyChar == '/' && ((sender as TextBox).Text.IndexOf("/") > -1 || !(sender as TextBox).Text.Any()))
                e.Handled = true;
            if ((sender as TextBox).Text.IndexOf(",") > -1 && (sender as TextBox).Text.IndexOf(",") + 3 < (sender as TextBox).Text.Count())
                e.Handled = true;

        }
        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == dataGridView.Columns["s"].Index && e.RowIndex > 0)
            {
                if (dataGridView[e.ColumnIndex, e.RowIndex].Value.ToString() == "≤") dataGridView[e.ColumnIndex, e.RowIndex].Value = "≥";
                else dataGridView[e.ColumnIndex, e.RowIndex].Value = "≤";
            }
            if (e.ColumnIndex == 0 && e.RowIndex == 0)
            {
                if (dataGridView[0, 0].Value.ToString() == "min")
                {
                    dataGridView[0, 0].Value = "max";
                    maximal = true;
                }
                else
                {
                    dataGridView[0, 0].Value = "min";
                    maximal = false;
                }
            }
        }
        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.FormattedValue.ToString().Contains('/'))
            {
                string value = e.FormattedValue.ToString();
                int index = value.IndexOf('/');
                string a = value.Substring(0,index);
                string b = value.Substring(index+1, value.Length-index-1);
                if (a.Equals("-")) a = "-1";
                if (a.Length == 0) a = "1";
                if (b.Length == 0) b = "1";
                dataGridView.EditingControl.Text = (Math.Round(Convert.ToDouble(a) / Convert.ToDouble(b),3)).ToString();
            }
            if (e.FormattedValue.ToString().Equals("-"))
                dataGridView.EditingControl.Text = "-1";
            if (e.FormattedValue.ToString().Length > 0 && e.FormattedValue.ToString()[0]==',')
                dataGridView.EditingControl.Text = "0," + e.FormattedValue.ToString().Substring(1);
        }
        private void dataGridView_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            int width = 0;
            foreach (DataGridViewColumn col in dataGridView.Columns)
                width += col.Width;
            this.dataGridView.Size = new Size(width+2, this.dataGridView.Size.Height);
        }
        private void dataGridView_RowHeightChanged(object sender, DataGridViewRowEventArgs e)
        {
            int height = 0;
            foreach (DataGridViewRow row in dataGridView.Rows)
                height += row.Height;
            this.dataGridView.Size = new Size(this.dataGridView.Size.Width, height+2);
        }

        /**START BUTTON
         */
        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView.ClearSelection();
            if (!this.tableLayoutPanel.Controls.Contains(groupBox5)) // to otwórz diagram w nowym oknie
            {
                diagramForm.Show();
            }
            panel.Controls.Clear();
            maximal = (dataGridView[0, 0].Value.ToString().Contains("min")) ? false : true;
            N = (int)upDown1Value; M = (int)upDown2Value;
            A = new decimal[M + 1, N + M + 1];
            richTextBox.Text = "";
            // take x0
            for (int i = 1, j = 1; i <= N * 2; i += 2)
            {
                if (dataGridView[i, 0].Value == null || String.IsNullOrWhiteSpace(dataGridView[i, 0].Value.ToString()))
                    dataGridView[i, 0].Value = 0;
                A[0, j] = Convert.ToDecimal(dataGridView[i, 0].Value);
                j++;
            }
            if (dataGridView[N * 2 + 2, 0].Value == null || String.IsNullOrWhiteSpace(dataGridView[N * 2 + 2, 0].Value.ToString()))
                   dataGridView[N * 2 + 2, 0].Value = 0;
            A[0, N + 1] = Convert.ToDecimal(dataGridView[N * 2 + 2, 0].Value);
            // take b
            for (int j = 1; j <= M; j++)
            {
                if (dataGridView[N * 2 + 2, j].Value == null || String.IsNullOrWhiteSpace(dataGridView[N * 2 + 2, j].Value.ToString()))
                    dataGridView[N * 2 + 2, j].Value = 0;
                A[j, 0] = Convert.ToDecimal(dataGridView[N * 2 + 2, j].Value);
            }
            // take A
            for (int i = 1, k = 1; i <= N * 2; i+=2)
            {
                for (int j = 1; j <= M; j++)
                {
                    if (dataGridView[i, j].Value == null || String.IsNullOrWhiteSpace(dataGridView[i, j].Value.ToString()))
                        dataGridView[i, j].Value = 0;
                    A[j, k] = Convert.ToDecimal(dataGridView[i, j].Value);
                }
                k++;
            }
            // adding I
            for (int i = N + 1; i <= N + M; i++)
                for (int j = 1; j <= M; j++)
                {
                    if (i - N == j)
                    {
                        if (dataGridView[N * 2 + 1, j].Value.Equals("≤"))
                            A[j, i] = 1;
                        else A[j, i] = -1;
                    }
                    else A[j, i] = 0;
                }

            // bazowa simpleksu 
            decimal[,] simp = new decimal[M + 2, N + 2];
            simp[0, 0] = 0; simp[0, 1] = 0; simp[1,0] = 0;
            for (int i = 1; i <= N; i++) simp[0, i + 1] = i;
            for (int i = 1; i <= M; i++) simp[i + 1, 0] = N + i;
            //przepisanie wartosci:
            simp[1, 1] = -A[0,0];
            for (int i = 1; i <= N; i++)
                if (maximal) simp[1, i + 1] = -A[0, i];
                else simp[1, i + 1] = A[0, i]; 
            for (int i = 1; i <= M; i++) 
            {
                int k = Convert.ToInt32(simp[i+1,0]);
                if (A[i, k] == -1)
                    simp[i + 1, 1] = -A[i, 0];
                else simp[i + 1, 1] = A[i, 0];
                for (int j = 1; j <= N; j++) 
                {
                    if (A[i, k] == -1) simp[i + 1, j + 1] = -A[i, j];
                    else simp[i + 1, j + 1] = A[i, j];
                }
            }
            
            // BEGIN ALGORYTHM
            BandB(simp);
        }

        /**BandB - breach and bound 
         * L - lista problemów
         * while (L zawiera problemy)
         *     - wybieramy z listy problem i go usuwamy
         *     - relaksacja:
         *     - rozwiązujemy problem zrelaksowany (simplex)
         *     (a) jeżeli 
         */ 
        private void BandB(decimal[,] y) 
        {
            Problem bestSol = new Problem() { id = -1, x0 = 0 };
            List<Problem> problemList = new List<Problem>(); // zawiera problemy już zakończone do diagramu 
            Queue<Simplex> problemQueue = new Queue<Simplex>(); // zawiera problemy w kolejce do rozwiązania
            problemQueue.Enqueue(new Simplex(y) { id = 0, res="" });
            int index = 0; decimal LB = Int64.MinValue;
            while (problemQueue.Any() && index < 100) 
            {
                if (problemList.Any() && problemList[0].info.Contains("NWN") && bestSol.id != -1) break;
                index++;
                Simplex P = problemQueue.Dequeue();
                richTextBox.Text += " --------------------------------------- \n  ID: " + P.id + ",   " + P.res + "\n";
                int k = (P.id + 1) * 2; // id podproblemów: k i k - 1
                richTextBox.Text += " Problem do rozwiązania: \n" + P.ToString();
                //rozwiązanie:
                Problem solution = P.Solve();
                solution.id = P.id;
                richTextBox.Text += "\n Rozwiązanie simpleks: \n" + P.ToString();
                if (solution.info != "") richTextBox.Text += "info: " + solution.info + "\n";
                solution.res = P.res;
                if (solution.x0 <= LB || solution.info.Contains("ZP") || solution.info.Contains("SP")) // dolne ograniczenie - koniec wierzchołka
                {
                    richTextBox.Text += " ---> koniec" + "\n";
                    if (!solution.info.Contains("ZP") && !solution.info.Contains("SP") && BandBStop(solution)) solution.info += "C";
                    if (!maximal) solution.x0 = -solution.x0;
                    problemList.Add(solution);
                    continue;
                }
                if (BandBStop(solution)) // całkowite - koniec wierzchołka
                {
                    if (solution.x0 > LB)
                    {
                        LB = solution.x0;
                        bestSol = solution;
                    }
                    richTextBox.Text += " ---> koniec (znalezione rozwiązanie)" + "\n";
                    solution.info += "C";
                    if (!maximal) solution.x0 = -solution.x0;
                    problemList.Add(solution);
                    if (problemList[0].info.Contains("NWO")) break;
                    continue;
                }
                if (!maximal) solution.x0 = -solution.x0;
                problemList.Add(solution);

                //relaksacja i podział na dwa przypadki:
                richTextBox.Text += "Podział względem:\n";
                int xIndex = 0; decimal xValue = Decimal.MinValue, xB = 0;
                for (int i = 0; i < solution.y.GetLength(1) - 2; i++) //tylko N pierwszych x'ów
                {
                    decimal f = solution.p[i] - Math.Floor(solution.p[i]);
                    if (Math.Min(f, 1 - f) > epsilon)
                    {
                        if ((maximal && (y[1, i + 2] < xB || xB == 0)) || (!maximal && (y[1, i + 2] > xB || xB == 0))) 
                        {
                            xB = y[1, i + 2];
                            xValue = solution.p[i];
                            xIndex = i;
                        }
                    }
                }
                xIndex++;
                richTextBox.Text += string.Format(" x{0} = {1:0.00} \n", xIndex, xValue);
                // dodaj pierwsze ograniczenie - mniejsze
                Simplex P1 = new Simplex(P.GetProblem()) { id = k - 1, y = P.y, res = "x" + xIndex + "≤" + Math.Floor(xValue) };
                P1.AddBound(xIndex, xValue, true);
                // dodaj drugie ograniczenie
                Simplex P2 = new Simplex(P.GetProblem()) { id = k, y = P.y, res = "x" + xIndex + "≥" + Math.Ceiling(xValue) };
                P2.AddBound(xIndex, xValue, false);
                //------------
                problemQueue.Enqueue(P1);
                problemQueue.Enqueue(P2);
            }
            if (bestSol.id < 0) // brak optymalnego
            {
                if (english) label5.Text = "No solution";
                else label5.Text = "Brak rozwiązania";
                label6.Text = "";
                label7.Text = "";
                if (index == 1 && (problemList[0].info.Contains("NWN") || problemList[0].info.Contains("NN")))
                    if (english) label7.Text = "Unbounded";
                    else label7.Text = "Zbiór nieograniczony";
            }
            else
            {
                label5.Text = string.Format("x0 = {0:0.####}", bestSol.x0);
                label6.Text = "x = ";//[" + sp + "]";
                label7.Text = "";
                if (problemList[0].info.Contains("NWN"))
                    label7.Text = "Rozwiązania leżą na zbiorze nieograniczonym";
                if (problemList[0].info.Contains("NWO"))// && problemList[0].info.Contains("C"))
                    label7.Text = "Wiele rozwiązań na zbiorze ograniczonym";
                foreach(var PROB in problemList)
                {
                    string sp = string.Concat(PROB.p.Take(PROB.y.GetLength(1) - 2).Select(i => string.Format("{0:0.##}  ", i)));
                        if (sp.Count() > 1) sp = sp.Remove(sp.Count() - 2);
                    if (PROB.x0==bestSol.x0 && PROB.info.Contains("C"))
                        label6.Text = label6.Text + "[" + sp + "]; ";
                }
            }
            DrawTreeDiagram(problemList.OrderBy(x => x.id).ToArray());

        }
        /**BandBStop - sprawdzenie całkowitoliczbowości
         */
        private bool BandBStop(Problem sol)
        {
            for (int i = 0; i < sol.y.GetLength(1) - 2; i++) // N
            {
                decimal f = sol.p[i] - Math.Floor(sol.p[i]);
                if (Math.Min(f, 1 - f) > epsilon)
                    return false;
            }
            return true;
        }

        /**DrawTreeDiagram
         * rysuje drzewo kolejnych rozwiązań i podziałów 
         */
        private void DrawTreeDiagram(Problem[] problemArray)
        {
            if (!problemArray.Any()) return;
            
            /*problemArray = new Problem[10];
            problemArray[0] = new Problem() { id = 0, x0 = 0, p = new List<decimal>() {1} };
            problemArray[1] = new Problem() { id = 1, x0 = 1, p = new List<decimal>() { 1 } };
            problemArray[2] = new Problem() { id = 2, x0 = 2, p = new List<decimal>() { 1 } };
            problemArray[3] = new Problem() { id = 3, x0 = 3, p = new List<decimal>() { 1 } };
            problemArray[4] = new Problem() { id = 4, x0 = 4, p = new List<decimal>() { 1 } };
            problemArray[5] = new Problem() { id = 5, x0 = 5, p = new List<decimal>() { 1 } };
            problemArray[6] = new Problem() { id = 8, x0 = 8, p = new List<decimal>() { 1 } };
            problemArray[7] = new Problem() { id = 9, x0 = 9, p = new List<decimal>() { 1 } };
            problemArray[8] = new Problem() { id = 10, x0 = 10, p = new List<decimal>() { 1 } };
            problemArray[9] = new Problem() { id = 7, x0 = 7, p = new List<decimal>() { 1 } };*/
            RichTextBox problemBox;
            Label labelBox;
            List<Problem> problemList = problemArray.ToList();
            BinaryTree tree = new BinaryTree(problemList);
            problemList = tree.Draw();
            int xj = boxWidth, yj = 15 + boxHeight;
            int minX = tree.minX;
            string str = "";
            foreach(var prob in problemList)
            {
                int x0 = (int)((prob.location.X - minX) * xj), y0 = (int)(prob.location.Y * yj);
                NewDiagramBox(out problemBox, prob.id);
                problemBox.Text = prob.ToString();
                problemBox.SelectAll();
                problemBox.SelectionIndent += 3;
                problemBox.SelectionRightIndent += 3;
                problemBox.SelectionLength = 0;
                panel.Controls.Add(problemBox);
                problemBox.Location = new Point(x0, y0);
                str += "(" + prob.id + ")" + x0 + "; ";
                if (prob.res != "")
                {
                    NewDiagramLabel(out labelBox, prob.id);
                    labelBox.Text = prob.res;
                    panel.Controls.Add(labelBox);
                    labelBox.Location = new Point(x0, y0 - 13);
                }
            }
            panel.Size = new Size(0, 0);
            panel.AutoSize = true;
            panel.Height = panel.Height + 15;
            richTextBox.Text += "\n\n";
        }
        // create and set new box
        //
        private void NewDiagramBox(out RichTextBox box, int nr)
        {
            box = new RichTextBox();
            box.Name = Convert.ToString(nr);
            box.BackColor = System.Drawing.SystemColors.Control;
            box.BorderStyle = System.Windows.Forms.BorderStyle.None;
            box.ReadOnly = true;
            box.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            box.Size = new System.Drawing.Size(boxWidth, boxHeight);
            box.MouseHover += textBox_MouseHover;
            box.MouseLeave += textBox_MouseLeave;
        }
        private void NewDiagramLabel(out Label lab, int nr)
        {
            lab = new Label();
            lab.Name = Convert.ToString(nr);
            lab.AutoSize = true;
            lab.BackColor = System.Drawing.SystemColors.Window;
            lab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            lab.Size = new System.Drawing.Size(27, 13);
        }
        private void textBox_MouseHover(object sender, EventArgs e)
        {
            var box = sender as RichTextBox;
            box.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            Size size = TextRenderer.MeasureText(box.Text, box.Font);
            box.Width = size.Width + 5;
            box.Height = size.Height + 5;
            box.ForeColor = System.Drawing.SystemColors.HotTrack;
            //box.BackColor = System.Drawing.SystemColors.Highlight;
            //box.BorderStyle = System.Windows.Forms.BorderStyle.None;
        }
        private void textBox_MouseLeave(object sender, EventArgs e)
        {
            var box = sender as RichTextBox;
            box.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            box.Size = new Size(boxWidth,boxHeight);
            box.ForeColor = System.Drawing.SystemColors.WindowText;
            //box.BackColor = System.Drawing.SystemColors.Control;
            //box.BorderStyle = System.Windows.Forms.BorderStyle.None;
        }

        // Painting lines between boxes
        //
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics gg = panel.CreateGraphics();
            gg.Clear(Color.White);
            List<RichTextBox> boxes = new List<RichTextBox>();
            foreach (Control box in panel.Controls)
            {
                if (box.GetType().ToString() == "System.Windows.Forms.RichTextBox")
                    boxes.Add(box as RichTextBox);
            }
            for (int i = 0; i < boxes.Count(); i++)
            {
                int k = Convert.ToInt32(boxes[i].Name);
                int j = (k + 1) * 2; //-1
                if (!boxes.Where(x => x.Name == j.ToString() || x.Name == (j-1).ToString()).Any())
                {
                    gg.DrawLine(new Pen(Color.Red, 2),
                        boxes[i].Location.X, boxes[i].Location.Y + boxHeight + 5,
                        boxes[i].Location.X + boxWidth, boxes[i].Location.Y + boxHeight + 5);
                }
                if (boxes[i].Name == "0") continue;
                j = (int)Math.Floor((Convert.ToDouble(k + 1) / 2) - 1);
                gg.DrawLine(new Pen(Color.Black, 2),
                    boxes[i].Location.X + boxWidth / 2, boxes[i].Location.Y + boxHeight / 2,
                    boxes.Where(x => x.Name == j.ToString()).First().Location.X + boxWidth / 2, 
                    boxes.Where(x => x.Name == j.ToString()).First().Location.Y + boxHeight / 2);
            }
        }
        private void diagramForm_FormClosing(object sender, FormClosingEventArgs e) 
        {
            e.Cancel = true;
            diagramForm.Hide();
        }
        // MENUSTRIP - VIEW
        //
        private void diagramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (diagramToolStripMenuItem.Checked)
            {
                this.tableLayoutPanel.Controls.Remove(groupBox5);
                diagramForm.Controls.Add(groupBox5);
                diagramToolStripMenuItem.Checked = false;
            }
            else
            {
                diagramForm.Hide();
                diagramForm.Controls.Remove(groupBox5);
                this.tableLayoutPanel.Controls.Add(groupBox5);
                diagramToolStripMenuItem.Checked = true;
            }
        }
        // MENUSTRIP - LANGUAGE
        //
        private void polskiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            english = false;
            this.polskiToolStripMenuItem.Checked = true;
            this.englishToolStripMenuItem.Checked = false;
            this.Text = "Programowanie Liniowe Całkowitoliczbowe - Metoda B&B";
            //menu
            this.viewToolStripMenuItem.Text = "Widok";
            this.settingsToolStripMenuItem.Text = "Ustawienia";
            this.aboutToolStripMenuItem.Text = "O programie";
            //this.propertiesToolStripMenuItem.Text = "Panel właściwości";
            this.diagramToolStripMenuItem.Text = "Panel diagramu";
            this.numericalPrecisionToolStripMenuItem.Text = "Numeryczna dokładność";
            this.languageToolStripMenuItem.Text = "Język";
            this.button.Text = "Rozwiąż";
            //boxes
            this.groupBox1.Text = "Właściwości";
            this.groupBox4.Text = "Wartości";
            this.groupBox5.Text = "Diagram";
            this.groupBox3.Text = "Rozwiązanie optymalne";
            this.groupBox2.Text = "Problemy przykładowe:";
            this.label1.Text = "Liczba zmiennych:";
            this.label2.Text = "Liczba ograniczeń:";
        }
        private void englishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            english = true;
            this.polskiToolStripMenuItem.Checked = false;
            this.englishToolStripMenuItem.Checked = true;
            this.Text = "Integral Linear Programing - B&B method";
            //menu
            this.viewToolStripMenuItem.Text = "View";
            this.settingsToolStripMenuItem.Text = "Settings";
            this.aboutToolStripMenuItem.Text = "About";
            //this.propertiesToolStripMenuItem.Text = "Properties Box";
            this.diagramToolStripMenuItem.Text = "Diagram Box";
            this.numericalPrecisionToolStripMenuItem.Text = "Numerical precision";
            this.languageToolStripMenuItem.Text = "Language";
            this.button.Text = "Solve";
            //boxes
            this.groupBox1.Text = "Properties";
            this.groupBox4.Text = "Values";
            this.groupBox5.Text = "Diagram";
            this.groupBox3.Text = "Optimal solution";
            this.groupBox2.Text = "Example problems:";
            this.label1.Text = "Number of parameters:";
            this.label2.Text = "Number of limitations:";
        }
        // MENUSTRIP - ABOUT
        // 
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool eng = true;
            if (polskiToolStripMenuItem.Checked) eng = false;
            AboutBox aboutBox = new AboutBox(eng);
            aboutBox.Show();
        }
        // MENUSTRIP - PRECISION
        // 
        private void epsilonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new PrecisionForm(english, epsilon);
            frm.ShowDialog();
            if (frm.accept)
                epsilon = frm.eps;
            
            epsilonToolStripMenuItem.Text = epsilon.ToString();
        }

        private void examplesGenerate(int nn, int mm, decimal[,] tab, bool maxmin)
        {
            if (maxmin) dataGridView[0, 0].Value = "max";
            else dataGridView[0, 0].Value = "min";
            for (int i = (int)upDown1Value; i < nn; i++) numericUpDown1.Value++;
            for (int i = (int)upDown1Value; i > nn; i--) numericUpDown1.Value--;
            for (int i = (int)upDown2Value; i < mm; i++) numericUpDown2.Value++;
            for (int i = (int)upDown2Value; i > mm; i--) numericUpDown2.Value--;
            for (int i = 0; i < nn; i++)
                for (int j = 0; j <= mm; j++)
                    dataGridView[i * 2 + 1, j].Value = tab[j, i];
            for (int j = 1; j <= mm; j++)
                dataGridView[nn * 2 + 2, j].Value = tab[j, nn];
            for (int i = 1; i <= (int)upDown2Value; i++)
                if (tab[i, nn + 1] == 0) dataGridView[nn * 2 + 1, i].Value = "≤";
                else dataGridView[nn * 2 + 1, i].Value = "≥";
        }
        private void examplesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel.Controls.Clear();
            label5.Text = "x0 =";
            label6.Text = "x =";
            label7.Text = "";
            richTextBox.Text = "";
            checkBox1.Checked = false;
            decimal[,] t; //m+1, n+2
            switch (exaplesListBox.SelectedIndex)
            {
                case 0: // z wykładu 5 // 2 x 3
                    t = new decimal[4, 4] { { 2, 1, 0, 0 }, { 1, 1, 5, 0 }, { -1, 1, 0, 0 }, { 6, 2, 21, 0 } };
                    examplesGenerate(2, 3, t, true);
                    break;
                case 1: // ---
                    t = new decimal[3, 4] { { 3, 4, 0, 0 },  { 2, 5, 15, 0 }, { 2, -2, 5, 0 } };
                    examplesGenerate(2, 2, t, true);
                    break;
                case 2: // z wykładu 7 - 2 x 3 (dwufazowe)
                    t = new decimal[4, 4] { { 1, 6, 0, 0 }, { 2, 1, 2, -1 }, { -1, 1, 3, 0 }, { 1, 1, 6, 0 } };
                    examplesGenerate(2, 3, t, true);
                    break;
                case 3: // z wykładu 8 - 2 x 3 (PLC)
                    t = new decimal[4, 4] { { 6, 5, 0, 0 }, { 9, 7, 63, 0 }, { 1, 1, 8, 0 }, { 3, 2, 6, -1 } };
                    examplesGenerate(2, 3, t, true);
                    break;
                case 4: // ze strony innej // 2 x 2
                    t = new decimal[3, 4] { { 5, 8, 0, 0 }, { 1, 1, 6, 0 }, { 5, 9, 45, 0 } };
                    examplesGenerate(2, 2, t, true);
                    break;
                case 5: // z wykładu 6 - zbiór pusty // 2 x 2
                    t = new decimal[3, 4] { { 1, 1, 0, 0 }, { -1, -1, 2, 0 }, { -1, 1, 1, 0 } };
                    examplesGenerate(2, 2, t, true);
                    break;
                case 6: // z wykładu 6 - nieskończenie wiele na ograniczonym // 2 x 2 
                    t = new decimal[3, 4] { { 4, 2, 0, 0 }, { -1, 1, 4, 0 }, { 2, 1, 6, 0 } };
                    examplesGenerate(2, 2, t, true);
                    break;
                case 7: // z wykładu 6 - nieskończenie wiele na nieograniczonym // 2 x 2
                    t = new decimal[3, 4] { { -2, 4, 0, 0 }, { -2, 1, 1, 0 }, { -1, 2, 4, 0 } };
                    examplesGenerate(2, 2, t, true);
                    break;
                case 8:
                    t = new decimal[4, 5] { { (decimal)(0.5), -1, -1, 0, 0 }, { -(decimal)(0.5), 2, 1, 2, 0 }, { -(decimal)(0.5), 2, -1, 3, -1  }, { 0, 1, -1, 2, 0 } };
                    examplesGenerate(3, 3, t, true);
                    break;
                case 9:
                    t = new decimal[4, 5] { { 1, 3, 2, 0, 0 }, { 1, 2, 1, 5, 0 }, { 1, 1, 1, 4, 0  }, { 0, 1, 2, 1, 0 } };
                    examplesGenerate(3, 3, t, true);
                    break;
                case 10: // -- 1: 2x3 --
                    t = new decimal[4, 4] { { 1, -1, 0, 0 }, { -2, 1, 3, 0 }, { 1, 1, 6, 0 }, { 5, 2, 20, 0 } };
                    examplesGenerate(2, 3, t, true);
                    break;
                case 11: // -- 2: 2x3 --
                    t = new decimal[4, 4] { { 1, 1, 0, 0 }, { -2, 1, 3, 0 }, { 1, 1, 6, 0 }, { 5, 2, 20, 0 } };
                    examplesGenerate(2, 3, t, true);
                    break;
                case 12: // -- 3: 2x3 --
                    t = new decimal[4, 4] { { 1, 2, 0, 0 }, { -2, 1, 3, 0 }, { 1, 1, 6, 0 }, { 5, 2, 20, 0 } };
                    examplesGenerate(2, 3, t, true);
                    break;
                case 13: // -- 4: 2x3 --
                    t = new decimal[4, 4] { { 1, -1, 0, 0 }, { -2, 1, 3, 0 }, { 1, 1, 6, 0 }, { 5, 2, 20, 0 } };
                    examplesGenerate(2, 3, t, false);
                    break;
                case 14: // -- 5: 2x2 --
                    t = new decimal[3, 4] { { 1, 2, 0, 0 }, { -2, 1, 2, -1 }, { 1, -2, 2, -1 } };
                    examplesGenerate(2, 2, t, false);
                    break;
                case 15: // -- 6: 4x5 --
                    t = new decimal[6, 6] { { 2, 1, 2, 2, 0, 0 }, { 1, 1, 1, 3, 5, 0 }, { -1, 1, 3, 2, 0, 0 }, { 6, 2, -4, 1, 21, 0 }, { -3, 2, 1, 1, 2, 0 }, { 1, -2, 3, 4, 5, 0 } };
                    examplesGenerate(4, 5, t, true);
                    break;
                default:
                    break;
            }
            exaplesListBox.ClearSelected();
        }

    }
}
