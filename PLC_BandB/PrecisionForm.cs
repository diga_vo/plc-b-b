﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLC_BandB
{
    public partial class PrecisionForm : Form
    {
        public decimal eps { get; set; }
        public bool accept { get; set; }
        private List<decimal> precisions = new List<decimal>() 
            { 1M/10, 1M/100, 1M/1000, 1M/10000, 1M/100000, 1M/1000000,
              1M/10000000, 1M/100000000, 1M/1000000000, 1M/10000000000};
        public PrecisionForm(bool eng, decimal e)
        {
            InitializeComponent();
            eps = e;
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox1.DataSource = precisions;
            accept = false;
            if (!eng)
            {
                Text = "Numeryczna dokładność";
                label1.Text = "Wybierz precyzję:";
            }
            //for (int i = 0; i < precisions.Count(); i++)
            {
               // if (precisions[i] == eps)
                comboBox1.SelectedIndex = comboBox1.FindStringExact(eps.ToString());

            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            eps = Convert.ToDecimal(comboBox1.SelectedValue);
            accept = true;
            this.Close();
        }

    }
}
