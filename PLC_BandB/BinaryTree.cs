﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLC_BandB
{
    class PointD
    {
        public double X { get; set; }
        public double Y { get; set; }
        public PointD(double x, double y)
        {
            X = x;
            Y = y;
        }
        public PointD(int x, int y)
        {
            X = (double)x;
            Y = (double)y;
        }
    }
    class BinaryTree
    {
        private List<Problem> Nodes;
        private int level;
        public int minX, maxX;
        public BinaryTree(List<Problem> N)
        {
            Nodes = N;
            Nodes = Nodes.OrderBy(x => x.id).ToList();
            level = (int)Math.Floor(Math.Log(Nodes.Last().id, 2));
            if (Nodes.Last().id == 0) level = 0;
        }
        /*public List<Problem> Draw()
        {
            Nodes[0].location = new Point(100, 0);
            minX = 100;
            for (int i = 0, p = 0, row = 0; p < Nodes.Count(); i++)
            {
                if (Math.Floor(Math.Log(i + 1, 2)) > row)
                    row++;

                if (Nodes[p].id == i)
                {
                    int k = 2 * (Nodes[p].id + 1), l1 = Nodes[p].location.X + 1, l2 = Nodes[p].location.X - 1;
                    if (Nodes.Where(x => x.id == k).Any()) // nowy w prawo
                        Nodes.Where(x => x.id == k).First().location = new Point(l1, row + 1);

                    if (Nodes.Where(x => x.id == k - 1).Any()) // nowy w lewo
                    {
                        var left = Nodes.Where(x => x.id == k - 1).First();
                        left.location = new Point(l2, row + 1);
                        minX = Math.Min(l2, minX);
                        //inny wierzchołek ma to połozenie
                        if (Nodes.Where(x => x.location.X > l2 && x.location.Y == (row + 1) && x.id < (k - 1)).Any())
                        {
                            
                            //MoveRight(FindRoot(k - 1), 2);
                            //var ps = Nodes.Where(x => x.location.X > l2 && x.location.Y == (row + 1) && x.id < (k - 1)).First();
                            //left.location = new Point(left.location.X + 1, left.location.Y);
                        }

                        if (Nodes.Where(x => x.location.X == left.location.X && x.location.Y == (row + 1) && x.id != (k - 1)).Any())
                        {
                            MessageBox.Show(i + " | " + (k - 1) + " " + l2 + " - "+ FindRoot(k-1));
                        
                            MoveRight(FindRoot(k - 1), 1);
                            
                            //var ps = Nodes.Where(x => x.location.X == left.location.X && x.location.Y == (row + 1) && x.id != (k - 1)).First();
                            //foreach (var prob in Nodes)
                            //    if (prob.location.X != -1 && prob.location.X >= l2 && prob.id != ps.id)
                            //        prob.location = new Point(prob.location.X + 1, prob.location.Y);
                        }

                    }
                    p++;
                }
            }

            return Nodes;
        }
        private int FindRoot(int id)
        {
            int r = (int)Math.Floor((double)(id - 1) / 2);
            if (r == 0) return id;
            return FindRoot(r);
        }
        private void Move(int root, int num)
        {
            if (root == 1) MoveRight(2, num);
            //MoveLeft()
        }
        private void MoveRight(int root, int num)
        {
            if (!Nodes.Where(x => x.id == root).Any()) return;
            var n = Nodes.Where(x => x.id == root).First();
            n.location = new Point(n.location.X + num, n.location.Y);
            int k = 2 * (root + 1);
            MoveRight(k, num);
            MoveRight(k - 1, num);
        }*/

        public List<Problem> Draw()
        {
            int sumW = 0;
            for (int i = level - 1; i >= 0; i--) sumW += (int)Math.Pow(2, i);
            int z = (int)Math.Pow(2, level + 1), x0 = 0, row = 0;
            minX = z;
            maxX = 0;
            string str = "";
            for (int p = 0, i = 0, j = 0, k = 0; p < Nodes.Count(); i++)
            {
                if (Math.Floor(Math.Log(i + 1, 2)) > k)
                {
                    row++; 
                    x0 = 0;
                    k++;
                    str += "\nr:" + row + " = ";
                    z = z / 2;
                    j = 0;
                }
                if (j == 0 && k == level) x0 = 0;               //pierwszy element w wierszu ostatnim
                else if (j == 0) x0 = (z - 1) / 2;              //pierwszy element w wierszu
                else x0 += z;                              
                if (Nodes[p].id == i)
                {
                    str += "(" + i + ")" + x0 + "; ";
                    Nodes[p].location = new PointD(x0, row);
                    minX = Math.Min(minX, x0);
                    maxX = Math.Max(maxX, x0);
                    p++;
                }
                j++;
            }
            //MessageBox.Show(str);
            AlignTree();
            return Nodes;
        }
        private void AlignTree()
        {
            // zmniejszenie dystansu
            for (int k = minX; k <= maxX; k++)
            {
                if (!Nodes.Where(x => x.location.X == k).Any())
                {
                    if (!Nodes.Where(x => x.location.X > k).Any()) break;
                    int next = (int)Nodes.Where(x => x.location.X > k).OrderBy(x => x.location.X).First().location.X;
                    next -= k;
                    //MessageBox.Show(" "+k + " " +next);
                    foreach (var n in Nodes)
                        if (n.location.X > k)
                            n.location = new PointD(n.location.X - next, n.location.Y);
                }
            }

            // wyśrodkowanie wierzchołków
            for (int k = Nodes.Count()-1; k>=0 && Nodes[k].location.Y == level; k--)
            {
                //MessageBox.Show(" " +level + " " + k);
                Center(Nodes[k].id);
            }
        }
        private void Center(int id)
        {
            if (id == 0) return;
            int root = (int)Math.Floor((double)(id - 1) / 2);
            int k1 = 2 * (root + 1), k2 = k1 - 1;
            if (Nodes.Where(x => x.id == k1).Any() && Nodes.Where(x => x.id == k2).Any())
                Nodes.Where(x => x.id == root).First().location.X = (Nodes.Where(x => x.id == k1).First().location.X + Nodes.Where(x => x.id == k2).First().location.X)/2;
            Center(root);
        }
    }
}
