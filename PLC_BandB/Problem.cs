﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC_BandB
{
    class Problem
    {
        public int id { get; set; }
        public decimal x0 { get; set; }
        public List<decimal> p { get; set; }
        public decimal[,] y { get; set; }
        public string info { get; set; }
        public string res { get; set; }
        public PointD location { get; set; }
        public Problem ()
        {
            info = "";
            res = "";
            p = new List<decimal>();
            x0 = 0;
            location = new PointD(-1, -1);
        }
        public override string ToString()
        {
            string sp = "";
            if (p.Count() >= 2) sp = string.Concat(p.Take(y.GetLength(1) - 2).Select(i => string.Format("{0:0.##}  ", i)));
            if (sp.Count() > 1) sp = sp.Remove(sp.Count() - 2);
            if (info.Contains("NWN") && info.Contains("ZP")) sp = "Zbiór nieograniczony";
            else if (info == "ZP") sp = "Zbiór\npusty";
            else if (info == "SP") sp = "Sprzeczność";
            //else if (info == "NWN") sp = "Nieograniczone";
            else sp = String.Format("x0= {0:0.##}\nx=[{1}]", x0, sp); 
            return sp;
        }

    }
}
